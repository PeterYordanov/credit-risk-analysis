﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Newtonsoft.Json;
using CreditRiskAnalysisWebAPI.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using CreditRiskAnalysisWebAPI.Options;
using Microsoft.Extensions.Logging;
using System;

namespace CreditRiskAnalysisWebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MediatorController : ControllerBase
    {
        private readonly ConfigurationOptions _configurationOptions;
        private readonly ILogger<MediatorController> _logger;

        public MediatorController(ILogger<MediatorController> logger, IOptions<ConfigurationOptions> configurationOptions)
        {
            _configurationOptions = configurationOptions.Value;
            _logger = logger;
        }

        /// <summary>
        /// For the given Mongo ObjectId, it will call the Quantum/ML Engine
        /// and make a prediction on all available models
        /// </summary>
        //https://localhost:44373/mediator/predictall?mongo_id=5fe8d0263ae68dda0a3b0953
        [HttpGet]
        [Route("/[controller]/PredictAll")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult PredictAll([FromQuery(Name = "MongoId")] string mongoId)
        {
            try
            {
                HttpResponseMessage responseMessage = default;

                using (var client = new HttpClient())
                {
                    responseMessage = client.GetAsync(_configurationOptions.BaseUrl + $"/model/predict_all?mongo_id={ mongoId }").Result;
                }

                string jsonResponse = responseMessage.Content.ReadAsStringAsync().Result;
                List<CoreApiResponse> model = JsonConvert.DeserializeObject<List<CoreApiResponse>>(jsonResponse);

                return Ok(model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Calls the Quantum/ML Engine to save graphs for data analysis
        /// </summary>
        [HttpGet]
        [Route("/[controller]/SaveGraphsS3")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult SaveGraphsS3()
        {
            try
            {
                HttpResponseMessage responseMessage = default;

                using (var client = new HttpClient())
                {
                    responseMessage = client.GetAsync(_configurationOptions.BaseUrl + "/save_graphs_s3").Result;
                }

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return BadRequest(ex.Message);
            }
        }
    }
}

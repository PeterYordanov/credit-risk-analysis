﻿using System;
using System.Collections.Generic;
using System.Linq;
using CreditRiskAnalysisWebAPI.Models;
using Newtonsoft.Json;
using CreditRiskAnalysisWebAPI.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Logging;
using MongoDB.Bson.Serialization.Conventions;
using CreditRiskAnalysisWebAPI.Helpers;

namespace CreditRiskAnalysisWebAPI.Controllers
{
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IMongoRepository<ClientCreditDetails> _mongoRepository;
        private readonly ILogger<MediatorController> _logger;

        public CustomersController(ILogger<MediatorController> logger, IMongoRepository<ClientCreditDetails> mongoRepository)
        {
            _logger = logger;
            _mongoRepository = mongoRepository;
            var conventions = new ConventionPack
            {
                new MemberMapConvention()
            };

            ConventionRegistry.Register(
               "LowerCaseConvention",
               conventions,
               t => true);
        }

        /// <summary>
        /// Insert Customer
        /// </summary>
        [HttpPost]
        [Route("/[controller]/Insert")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Insert([FromBody] JObject received)
        {
            try
            {
                ClientCreditDetails clientCreditDetails = JsonConvert.DeserializeObject<ClientCreditDetails>(received.ToString());

                _mongoRepository.InsertOne(clientCreditDetails);

                return Ok(clientCreditDetails);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deletes all entries in the database
        /// </summary>
        [HttpPost]
        [Route("/[controller]/DeleteAll")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult DeleteAll()
        {
            try
            {
                List<ClientCreditDetails> clientCreditDetails = _mongoRepository.FilterBy(x => x.Age < 99 && x.Age >= 18).ToList();

                foreach(var item in clientCreditDetails)
                {
                    string id = item.Id.ToString();
                    _mongoRepository.DeleteById(id);
                }

                return Ok(new List<ClientCreditDetails>());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Lists all entries of the database
        /// </summary>
        [HttpGet]
        [Route("/[controller]/List")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult ListAll()
        {
            try
            {
                List<ClientCreditDetails> clientCreditDetails = _mongoRepository.FilterBy(x => x.Age < 99 && x.Age >= 18).ToList();

                return Ok(clientCreditDetails);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Gets information about specific customer
        /// </summary>
        [HttpGet]
        [Route("/[controller]/Get")]
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetCustomer([FromQuery(Name = "MongoId")] string mongoId)
        {
            try
            {
                ClientCreditDetails clientCreditDetails = _mongoRepository.FindById(mongoId);

                return Ok(clientCreditDetails);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return BadRequest(ex.Message);
            }
        }
    }
}

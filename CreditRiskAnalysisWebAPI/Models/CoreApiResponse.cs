using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;

namespace CreditRiskAnalysisWebAPI.Models
{
    [SwaggerSchema(Description = "Response from Quantum Computing & Machine Learning Core Engine")]
    public class CoreApiResponse
    {
        [SwaggerSchema("The type of the Machine Learning model used")]
        [JsonProperty(PropertyName = "ModelType")]
        public string ModelType { get; set; }
        [SwaggerSchema("Predicted value (0 or 1)")]
        [JsonProperty(PropertyName = "Predicted")]
        public int? Predicted { get; set; }
        [SwaggerSchema("Predicted value, confidence between 0 and 1")]
        [JsonProperty(PropertyName = "PredictedProbability")]
        public decimal? PredictedProbability { get; set; }
        [SwaggerSchema("ROC-AUC Score")]
        [JsonProperty(PropertyName = "RocAucScore")]
        public decimal? RocAucScore { get; set; }
        [SwaggerSchema("Response Status")]
        public string Status { get; set; }
        [SwaggerSchema("Accuracy of the model against the testing data")]
        public string AccuracyScore { get; set; }
    }
}

﻿using MongoDB.Bson;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using System;

namespace CreditRiskAnalysisWebAPI.Models
{
    [SwaggerSchema(Description = "Model to store the personal and financial data of customers")]
    public class ClientCreditDetails : IDocument
    {
        [SwaggerSchema("Auto-generated Identifier", ReadOnly = true)]
        public ObjectId Id { get; set; }
        [SwaggerSchema("The date of created entry", Format = "date")]
        [JsonIgnore]
        public DateTime CreatedAt => Id.CreationTime;
        [SwaggerSchema("First Name of borrower")]
        public string FirstName { get; set; }
        [SwaggerSchema("Last Name of borrower")]
        public string LastName { get; set; }
        [SwaggerSchema("Email of borrower")]
        public string Email { get; set; }
        [SwaggerSchema("Gender of borrower")]
        public string Gender { get; set; }
        [SwaggerSchema("Age of borrower")]
        public int Age { get; set; }
        public decimal RevolvingUtilizationOfUnsecuredLines { get; set; }
        public int NumberOfTime3059DaysPastDueNotWorse { get; set; }
        public decimal DebtRatio { get; set; }
        public decimal MonthlyIncome { get; set; }
        public int NumerOfOpenCreditLinesAndLoans { get; set; }
        public int NumberOfTimes90DaysLate { get; set; }
        public int NumberOfRealEstateLoansOrLines { get; set; }
        public int NumberOfTime6089DaysPastDueNotWorse { get; set; }
        public int NumberOfDependents { get; set; }
    }
}

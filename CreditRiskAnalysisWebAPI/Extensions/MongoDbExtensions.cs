﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Linq;

namespace CreditRiskAnalysisWebAPI.Extensions
{
    public static class MongoDbExtensions
    {
        public static bool CollectionExists(this IMongoDatabase database, string collectionName)
        {
            return database.ListCollectionsAsync().Result.Any();
        }
    }
}

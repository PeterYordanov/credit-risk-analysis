﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;

namespace CreditRiskAnalysisWebAPI.Helpers
{
    public class MemberMapConvention : IMemberMapConvention
    {
        private string _name = "LowerCaseConvention";

        #region Implementation of IConvention

        public string Name
        {
            get { return _name; }
            private set { _name = value; }
        }

        public void Apply(BsonMemberMap memberMap)
        {
            if (memberMap.MemberName == "Text")
            {
                memberMap.SetElementName("NotText");
            }
        }

        #endregion
    }
}

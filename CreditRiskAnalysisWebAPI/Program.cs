using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace CreditRiskAnalysisWebAPI
{
#pragma warning disable CS1591
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureAppConfiguration((context, builder) =>
                {
                    string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                    context.HostingEnvironment.EnvironmentName = environment;
                    Console.WriteLine($"Env: { context.HostingEnvironment.EnvironmentName }");

                    builder.SetBasePath(AppContext.BaseDirectory)
                            .AddJsonFile($"appsettings.{ context.HostingEnvironment.EnvironmentName }.json", true, true);
                });
    }
#pragma warning restore CS1591
}

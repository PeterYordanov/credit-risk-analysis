# Credit Risk Analysis

[[_TOC_]]

## CI/CD
[![pipeline status](https://gitlab.com/PeterYordanov/credit-risk-analysis/badges/master/pipeline.svg)](https://gitlab.com/PeterYordanov/calculator/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
  
<p>
Depends on https://gitlab.com/PeterYordanov/machine-learning-quantum-engine-api
</p>

![Recording](screenshots/Recording.gif "Recording")

## Tech Stack (incl. Machine Learning & Quantum Engine API)
- UIKit
- ASP.NET Core
- Swagger
- MongoDB
- Qiskit
- numpy
- matplotlib
- sklearn
- Flask
- boto
- logger
- AppMetrics
- Selenium

## System Design
```mermaid
graph TB
  subgraph "Credit Risk Analysis Website"
  UI("Credit Risk Analysis UI (UIKit, ASP.NET Core)")
  UI -- invokes --> API("Credit Risk Analysis API (ASP.NET Core, MongoDriver)")
  API -- invokes --> Engine("Machine Learning & Quantum Engine API (Flask, Qiskit, sklearn)")
  end

  subgraph "Credit Risk Analysis Bot"
  BOT("Credit Risk Analysis Bot (Selenium)")
  BOT -- executes actions on --> UI
  end

  subgraph "Database"
  DB("Mongo Database")
  API -- queries --> DB
end
```

## Features
- [x] Graphs
- [x] Accordion
- [x] Operations
   - [x] Add user
   - [x] Delete all
   - [x] Insert semi-randomly generated data
   - [x] Details
- [x] Algorithms
   - [x] Quantum Support Vector Machine
   - [x] Support Vector Machine
   - [x] KNN
   - [x] Logistic Regression
   - [x] Ada Boost
   - [x] Gaussian Naive Bayes
   - [x] Gradient Boost
   - [x] Random Boost
   - [x] MLP
   - [x] Decision Tree
   - [x] Extra Trees

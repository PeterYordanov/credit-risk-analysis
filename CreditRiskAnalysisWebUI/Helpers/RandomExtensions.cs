﻿using System;

namespace CreditRiskAnalysisWebUI.Helpers
{
    public static class RandomExtensions
    {
        public static decimal NextDecimal(this Random random, decimal min, decimal max)
        {
            decimal val = ((decimal)random.NextDouble() * (max - min) + min);
            return (decimal)val;
        }
    }
}

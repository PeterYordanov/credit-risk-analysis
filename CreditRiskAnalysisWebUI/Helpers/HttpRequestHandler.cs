﻿using CreditRiskAnalysisWebUI.Models;
using CreditRiskAnalysisWebUI.Options;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CreditRiskAnalysisWebAPI.Helpers
{
    public interface IHttpRequestHandler
    {
        Task Insert(string endpoint, ClientViewModel model);
        Task<List<ClientViewModel>> List(string endpoint);
        Task<ClientViewModel> GetById(string endpoint, string id);
        Task DeleteAll(string endpoint);
        Task<List<PredictionResultsViewModel>> PerformAnalysis(string endpoint, string id);
    }

    public class HttpRequestHandler : IHttpRequestHandler,
                                      IDisposable
    {
        private readonly HttpClient _httpClient;
        private bool disposedValue;
        private string _baseUrl;

        public HttpRequestHandler(IOptions<ConfigurationOptions> options)
        {
            _httpClient = new HttpClient();
            _baseUrl = options.Value.BaseUrl;
        }

        public async Task DeleteAll(string endpoint)
        {
            await _httpClient.GetAsync(string.Format("{0}/{1}", _baseUrl, endpoint));
        }

        public async Task<ClientViewModel> GetById(string endpoint, string id)
        {
            HttpResponseMessage responseMessage = await _httpClient.GetAsync(string.Format("{0}/{1}", _baseUrl, endpoint + $"?MongoId={ id }"));

            string jsonResponse = responseMessage.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<ClientViewModel>(jsonResponse);
        }

        public async Task Insert(string endpoint, ClientViewModel model)
        {
            //JsonSerializerSettings settings = new JsonSerializerSettings()
            //{
            //    Formatting = Formatting.Indented,
            //    NullValueHandling = NullValueHandling.Ignore,
            //};

            //string json = JsonConvert.SerializeObject(model, settings);
            //StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            //HttpResponseMessage httpResponseMessage = await _httpClient.PostAsync(string.Format("{0}/{1}", _baseUrl, endpoint), stringContent);
            //HttpResponseMessage responseMessage = default;

            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
            };

            string json = JsonConvert.SerializeObject(model, settings);
            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage responseMessage = _httpClient.PostAsync(string.Format("{0}/{1}", _baseUrl, endpoint), stringContent).Result;
        }

        public async Task<List<ClientViewModel>> List(string endpoint)
        {
            HttpResponseMessage responseMessage = await _httpClient.GetAsync(string.Format("{0}/{1}", _baseUrl, endpoint));

            string jsonResponse = responseMessage.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<List<ClientViewModel>>(jsonResponse);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                _httpClient.Dispose();
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public async Task<List<PredictionResultsViewModel>> PerformAnalysis(string endpoint, string id)
        {
            HttpResponseMessage responseMessage = await _httpClient.GetAsync(string.Format("{0}/{1}", _baseUrl, endpoint + $"?MongoId={ id }"));

            string jsonResponse = responseMessage.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<List<PredictionResultsViewModel>>(jsonResponse);
        }
    }
}

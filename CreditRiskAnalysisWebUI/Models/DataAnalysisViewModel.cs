﻿using Amazon.S3.Model;
using System.Collections.Generic;

namespace CreditRiskAnalysisWebUI.Models
{
    public class DataAnalysisViewModel
    {
        public List<S3Object> ObjectsList { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace CreditRiskAnalysisWebUI.Models
{
    public class PredictionResultsViewModel
    {
        [JsonProperty(PropertyName = "ModelType")]
        public string ModelType { get; set; }
        [JsonProperty(PropertyName = "Predicted")]
        public int? Predicted { get; set; }
        [JsonProperty(PropertyName = "PredictedProbability")]
        public decimal? PredictedProbability { get; set; }
        [JsonProperty(PropertyName = "RocAucScore")]
        public decimal? RocAucScore { get; set; }
        public string Status { get; set; }
        public string AccuracyScore { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace CreditRiskAnalysisWebUI.Models
{
    public class CustomerPredictionViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public decimal RevolvingUtilizationOfUnsecuredLines { get; set; }
        public int NumberOfTime3059DaysPastDueNotWorse { get; set; }
        public decimal DebtRatio { get; set; }
        public decimal MonthlyIncome { get; set; }
        public int NumberOfOpenCreditLinesAndLoans { get; set; }
        public int NumberOfTimes90DaysLate { get; set; }
        public int NumberOfRealEstateLoansOrLines { get; set; }
        public int NumberOfTime6089DaysPastDueNotWorse { get; set; }
        public int NumberOfDependents { get; set; }
        public List<PredictionResultsViewModel> Predictions { get; set; }
    }
}

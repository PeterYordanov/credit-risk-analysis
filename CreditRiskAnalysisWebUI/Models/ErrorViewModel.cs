using System;

namespace CreditRiskAnalysisWebUI.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
        public string ErrorMessage { get; set; }
        public string HResult { get; set; }
        public string InnerException { get; set; }
        public string BaseException { get; set; }
        public string StackTrace { get; set; }
        public string ExceptionType { get; set; }
    }
}

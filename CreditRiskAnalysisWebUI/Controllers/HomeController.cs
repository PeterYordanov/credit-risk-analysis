﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CreditRiskAnalysisWebUI.Models;
using Amazon.S3.Model;
using Amazon.S3;
using Amazon;

namespace CreditRiskAnalysisWebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            AmazonS3Client client = new AmazonS3Client("AKIAIRTN5WSMDKWHJ5OA", "g8m221knU/Ob94iCqbseQJY3o9ddw662Io6KLrPU", RegionEndpoint.EUWest1);
            ListObjectsV2Request request = new ListObjectsV2Request
            {
                BucketName = "credit-risk-analysis"
            };

            ListObjectsV2Response response = client.ListObjectsV2Async(request).Result;
            List<S3Object> objects = response.S3Objects;

            _logger.LogInformation("Visited Home Page");

            return View(new DataAnalysisViewModel { ObjectsList = objects });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string errorMessage, 
                                   string hResult, 
                                   string innerException, 
                                   string baseException,
                                   string stackTrace,
                                   string exceptionType)
        {
            ErrorViewModel errorViewModel = new ErrorViewModel
            {
                ErrorMessage = errorMessage,
                HResult = hResult,
                InnerException = string.IsNullOrEmpty(innerException) ? "" : innerException,
                StackTrace = stackTrace,
                ExceptionType = exceptionType,
                BaseException = baseException
            };

            _logger.LogInformation("Visited Error Page");
            errorViewModel.RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;

            return View(errorViewModel);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using CreditRiskAnalysisWebUI.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AutoMapper;
using CreditRiskAnalysisWebUI.Options;
using Microsoft.Extensions.Options;
using CreditRiskAnalysisWebAPI.Helpers;
using System.Threading.Tasks;
using CreditRiskAnalysisWebUI.Helpers;

namespace CreditRiskAnalysisWebUI.Controllers
{
    public class CreditRiskAnalysisController : Controller
    {
        private readonly ILogger<CreditRiskAnalysisController> _logger;
        private readonly IMapper _mapper;
        private readonly ConfigurationOptions _configurationOptions;
        private readonly IHttpRequestHandler _httpRequestHandler;

        public CreditRiskAnalysisController(ILogger<CreditRiskAnalysisController> logger, IMapper mapper, IOptions<ConfigurationOptions> configurationOptions, IHttpRequestHandler httpRequestHandler)
        {
            _logger = logger;
            _mapper = mapper;
            _configurationOptions = configurationOptions.Value;
            _httpRequestHandler = httpRequestHandler;
        }

        public async Task<IActionResult> Index()
        {
            try
            {
                List<ClientViewModel> list = await _httpRequestHandler.List("customers/list");

                return View(list);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return RedirectToAction("Error", "Home", new
                {
                    ErrorMessage = ex.Message,
                    ex.HResult,
                    InnerException = ex.InnerException?.Message,
                    BaseException = ex.GetBaseException().Message,
                    ex.StackTrace,
                    ExceptionType = ex.GetType().Name
                });
            }
        }

        public IActionResult Randomize()
        {
            List<ClientViewModel> clientData = new List<ClientViewModel>
            {
                new ClientViewModel
                {
                    FirstName = "Peter",
                    LastName = "Yordanov",
                    Age = 22,
                    Email = "peter.yordanov0@gmail.com",
                    Gender = "Male",
                    DebtRatio = new Random().NextDecimal(0.000000000M, 1M),
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumberOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = new Random().NextDecimal(0.000000000M, 1M)
                },
                new ClientViewModel
                {
                    FirstName = "Jane",
                    LastName = "Doe",
                    Age = new Random().Next(18, 90),
                    Email = "jane.doe@gmail.com",
                    Gender = "Female",
                    DebtRatio = new Random().NextDecimal(0.000000000M, 1M),
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumberOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = new Random().NextDecimal(0.000000000M, 1M)
                },
                new ClientViewModel
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Age = new Random().Next(18, 90),
                    Email = "john.doe@gmail.com",
                    Gender = "Male",
                    DebtRatio = new Random().NextDecimal(0.000000000M, 1M),
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumberOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = new Random().NextDecimal(0.000000000M, 1M)
                },
                new ClientViewModel
                {
                    FirstName = "John",
                    LastName = "Shaw",
                    Age = new Random().Next(18, 90),
                    Email = "john.shaw@gmail.com",
                    Gender = "Male",
                    DebtRatio = new Random().NextDecimal(0.000000000M, 1M),
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumberOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = new Random().NextDecimal(0.000000000M, 1M)
                },
                new ClientViewModel
                {
                    FirstName = "Natalie",
                    LastName = "Ng",
                    Age = new Random().Next(18, 90),
                    Email = "natalie.ng@gmail.com",
                    Gender = "Female",
                    DebtRatio = new Random().NextDecimal(0.000000000M, 1M),
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumberOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = new Random().NextDecimal(0.000000000M, 1M)
                },
                new ClientViewModel
                {
                    FirstName = "Emily",
                    LastName = "Brendon",
                    Age = new Random().Next(18, 90),
                    Email = "emily.brendon@gmail.com",
                    Gender = "Female",
                    DebtRatio = new Random().NextDecimal(0.000000000M, 1M),
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumberOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = new Random().NextDecimal(0.000000000M, 1M)
                },
            };

            foreach(var client in clientData)
            {
                _httpRequestHandler.Insert("customers/insert", client);
            }

            return RedirectToAction("Index", "CreditRiskAnalysis");
        }

        public async Task<IActionResult> Details(string id)
        {
            try
            {
                //HttpResponseMessage responseMessageDetails = default;

                //using (var client = new HttpClient())
                //{
                //    responseMessageDetails = client.GetAsync(_configurationOptions.BaseUrl + $"/Customers/Get?MongoId={ id }").Result;
                //}

                //string jsonResponseDetails = responseMessageDetails.Content.ReadAsStringAsync().Result;

                //ClientViewModel clientCreditDetails = JsonConvert.DeserializeObject<ClientViewModel>(jsonResponseDetails);

                ClientViewModel clientViewModel = await _httpRequestHandler.GetById("customers/get", id);

                return View(clientViewModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return RedirectToAction("Error", "Home", new
                {
                    ErrorMessage = ex.Message,
                    ex.HResult,
                    InnerException = ex.InnerException?.Message,
                    BaseException = ex.GetBaseException().Message,
                    ex.StackTrace,
                    ExceptionType = ex.GetType().Name
                });
            }
        }

        public async Task<IActionResult> PerformAnalysis(string id)
        {
            try
            {
                ClientViewModel clientViewModel = await _httpRequestHandler.GetById("customers/get", id);
                List<PredictionResultsViewModel> predictionResultsViewModel = await _httpRequestHandler.PerformAnalysis("mediator/predictall", id);

                //HttpResponseMessage responseMessagePrediction = default;
                //HttpResponseMessage responseMessageDetails = default;

                //using (var client = new HttpClient())
                //{
                //    responseMessageDetails = client.GetAsync(_configurationOptions.BaseUrl + $"/Customers/Get?MongoId={ id }").Result;
                //    responseMessagePrediction = client.GetAsync(_configurationOptions.BaseUrl + $"/Mediator/PredictAll?MongoId={ id }").Result;
                //}

                //string jsonResponsePrediction = responseMessagePrediction.Content.ReadAsStringAsync().Result;
                //string jsonResponseDetails = responseMessageDetails.Content.ReadAsStringAsync().Result;

                //List<PredictionResultsViewModel> predictionResults = JsonConvert.DeserializeObject<List<PredictionResultsViewModel>>(jsonResponsePrediction);
                //ClientViewModel clientCreditDetails = JsonConvert.DeserializeObject<ClientViewModel>(jsonResponseDetails);

                CustomerPredictionViewModel customerPredictionViewModel = _mapper.Map<CustomerPredictionViewModel>(clientViewModel);

                PredictionResultsViewModel svm = predictionResultsViewModel.Where(x => x.ModelType.Equals("SVC")).FirstOrDefault();
                //Change name from Support Vector Classifier (SVC) to Support Vector Machine (SVM)
                List<PredictionResultsViewModel> temp = new List<PredictionResultsViewModel>();
                foreach(var item in predictionResultsViewModel)
                {
                    if(item.ModelType.Equals("SVC"))
                    {
                        item.ModelType = "SVM";
                    }
                    temp.Add(item);
                }

                customerPredictionViewModel.Predictions = temp;

                return View(customerPredictionViewModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return RedirectToAction("Error", "Home", new
                {
                    ErrorMessage = ex.Message,
                    ex.HResult,
                    InnerException = ex.InnerException?.Message,
                    BaseException = ex.GetBaseException().Message,
                    ex.StackTrace,
                    ExceptionType = ex.GetType().Name
                });
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Clients()
        {
            _logger.LogInformation("Visited Clients Page");

            return View();
        }

        public ActionResult EditClient()
        {
            _logger.LogInformation("Visited Edit Client Page");

            return View();
        }

        public ActionResult CreateClient()
        {
            _logger.LogInformation("Visited Create Client Page");

            return View();
        }

        [HttpPost]
        public IActionResult CreateClient(ClientViewModel clientCreditDetails)
        {
            try
            {
                //HttpResponseMessage responseMessage = default;

                //using (var client = new HttpClient())
                //{
                //    JsonSerializerSettings settings = new JsonSerializerSettings()
                //    {
                //        Formatting = Formatting.Indented,
                //        NullValueHandling = NullValueHandling.Ignore,
                //    };

                //    string json = JsonConvert.SerializeObject(clientCreditDetails, settings);
                //    StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                //    responseMessage = client.PostAsync(_configurationOptions.BaseUrl + "/Customers/Insert", stringContent).Result;
                //}

                _httpRequestHandler.Insert("customers/insert", clientCreditDetails);

                return RedirectToAction("Index", "CreditRiskAnalysis");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return RedirectToAction("Error", "Home", new
                {
                    ErrorMessage = ex.Message,
                    ex.HResult,
                    InnerException = ex.InnerException?.Message,
                    BaseException = ex.GetBaseException().Message,
                    ex.StackTrace,
                    ExceptionType = ex.GetType().Name
                });
            }
        }

        public IActionResult DeleteAll()
        {
            try
            {
                HttpResponseMessage responseMessage = default;

                using (var client = new HttpClient())
                {
                    StringContent stringContent = new StringContent(JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json");
                    responseMessage = client.PostAsync(_configurationOptions.BaseUrl + "/Customers/DeleteAll", stringContent).Result;
                }

                return RedirectToAction("Index", "CreditRiskAnalysis");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return RedirectToAction("Error", "Home", new
                {
                    ErrorMessage = ex.Message,
                    ex.HResult,
                    InnerException = ex.InnerException?.Message,
                    BaseException = ex.GetBaseException().Message,
                    ex.StackTrace,
                    ExceptionType = ex.GetType().Name
                });
            }
        }
    }
}

using AutoMapper;
using CreditRiskAnalysisWebAPI.Helpers;
using CreditRiskAnalysisWebUI.Models;
using CreditRiskAnalysisWebUI.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CreditRiskAnalysisWebUI
{
    public class Mappings : Profile
    {
        public Mappings()
        {
            CreateMap<CustomerPredictionViewModel, ClientViewModel>();
            CreateMap<ClientViewModel, CustomerPredictionViewModel>();
            CreateMap<CustomerPredictionViewModel, PredictionResultsViewModel>();
            CreateMap<PredictionResultsViewModel, CustomerPredictionViewModel>();
        }
    }

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddMvc();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new Mappings());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddOptions<ConfigurationOptions>().Configure(options => Configuration.GetSection(nameof(ConfigurationOptions)).Bind(options));
            services.AddScoped<IHttpRequestHandler, HttpRequestHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "bank",
                    pattern: "{controller=Bank}/{action=Index}/{id?}"
                    );
            });
        }
    }
}

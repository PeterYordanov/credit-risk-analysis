﻿using CommandLine;

namespace CreditRiskAnalysisBot.Models
{
    public class CommandLineArguments
    {
        [Option('i', "inert_data_only", Required = false, HelpText = "", Default = true)]
        public bool InsertDataOnly { get; set; }

        [Option('u', "url", Required = false, HelpText = "", Default = "https://localhost:44356/")]
        public string Url { get; set; }
    }
}

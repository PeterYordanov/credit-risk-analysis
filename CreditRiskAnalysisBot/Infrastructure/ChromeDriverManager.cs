﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using WebDriverManager;

namespace CreditRiskAnalysisBot.Infrastructure
{
    public class ChromeDriverManager
    {
        public static void CloseDriverInstances()
        {
            foreach (Process currentProcess in Process.GetProcesses())
            {
                if (currentProcess.ProcessName.Trim().ToLower().Equals("chromedriver"))
                {
                    currentProcess.Kill();
                }
            }
        }

        public static string GetCompatibleDriverVersion()
        {
            string exactVersion;
            const string suffix = @"Google\Chrome\Application\chrome.exe";
            List<string> prefixes = new List<string> { Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) };
            string programFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
            string programFilesx86 = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
            if (programFilesx86 != programFiles)
            {
                prefixes.Add(programFiles);
            }
            else
            {
                string programFilesDirFromReg = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion", "ProgramW6432Dir", null) as string;
                if (!string.IsNullOrEmpty(programFilesDirFromReg))
                {
                    prefixes.Add(programFilesDirFromReg);
                }
            }

            prefixes.Add(programFilesx86);
            string path = prefixes.Distinct().Select(prefix => Path.Combine(prefix, suffix)).FirstOrDefault(File.Exists);
            string version = FileVersionInfo.GetVersionInfo(path.ToString()).FileVersion;
            string[] array = version.Split('.');
            array = array.Take(array.Count() - 1).ToArray();
            string partialVersion = string.Join(".", array);
            WebRequest req = WebRequest.Create("https://chromedriver.storage.googleapis.com/");
            req.Method = "GET";
            using (StreamReader reader = new StreamReader(req.GetResponse().GetResponseStream()))
            {
                string source = reader.ReadToEnd();
                XmlDocument xmltest = new XmlDocument();
                xmltest.LoadXml(source);
                XmlNodeList elemlist = xmltest.GetElementsByTagName("Key");
                var keys = elemlist.Cast<XmlNode>().Select(p => p.InnerText).Where(p => p.Contains("_win32.zip")).ToList();
                var filteredKeys = keys.Where(p => p.Contains($"{partialVersion}")).ToList();
                exactVersion = filteredKeys.LastOrDefault();
            }

            return exactVersion;
        }

        public static string GetCompatibleDriverUrl()
        {
            return $"https://chromedriver.storage.googleapis.com/{ GetCompatibleDriverVersion() }";
        }

        public static void DownloadDriver(string path)
        {
            string driverVersion = GetCompatibleDriverVersion();
            new DriverManager().SetUpDriver(GetCompatibleDriverUrl(), Path.Combine(path, "chromedriver.exe"), "chromedriver.exe");
        }
    }
}

﻿namespace CreditRiskAnalysisBot.Infrastructure
{
    public class BasePage
    {
        protected WebDriverFacade WebDriver { get; set; }

        public BasePage(WebDriverFacade webDriver)
        {
            this.WebDriver = webDriver;
        }
    }
}
﻿using CommandLine;
using CreditRiskAnalysisBot.Application;
using CreditRiskAnalysisBot.Infrastructure;
using CreditRiskAnalysisBot.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CreditRiskAnalysisBot
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<CommandLineArguments>(args)
                               .WithParsed(o =>
                               {
                                   CreateHost(o).Wait();
                               });
        }

        static Task CreateHost(CommandLineArguments commandLineArguments)
        {
            ChromeDriverManager.CloseDriverInstances();

            if (!File.Exists("chromedriver.exe"))
            {
                ChromeDriverManager.DownloadDriver(Directory.GetCurrentDirectory());
            }

            CreditRiskModellingBot bot = new CreditRiskModellingBot();

            //Dummy data
            List<ClientData> clientData = new List<ClientData>
            {
                new ClientData
                {
                    FirstName = "Peter",
                    LastName = "Yordanov",
                    Age = 22,
                    Email = "peter.yordanov0@gmail.com",
                    Gender = "Male",
                    DebtRatio = 0.298354075M,
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumerOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = 0.010351857M
                },
                new ClientData
                {
                    FirstName = "Jane",
                    LastName = "Doe",
                    Age = new Random().Next(18, 90),
                    Email = "jane.doe@gmail.com",
                    Gender = "Female",
                    DebtRatio = 0.606290901M,
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumerOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = 0.189169052M
                },
                new ClientData
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Age = new Random().Next(18, 90),
                    Email = "john.doe@gmail.com",
                    Gender = "Male",
                    DebtRatio = 0.241621845M,
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumerOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = 0.04656027M
                },
                new ClientData
                {
                    FirstName = "John",
                    LastName = "Shaw",
                    Age = new Random().Next(18, 90),
                    Email = "john.shaw@gmail.com",
                    Gender = "Male",
                    DebtRatio = 75M,
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumerOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = 1M
                },
                new ClientData
                {
                    FirstName = "Natalie",
                    LastName = "Ng",
                    Age = new Random().Next(18, 90),
                    Email = "natalie.ng@gmail.com",
                    Gender = "Female",
                    DebtRatio = 10M,
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumerOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = 0.24535301M
                },
                new ClientData
                {
                    FirstName = "Emily",
                    LastName = "Brendon",
                    Age = new Random().Next(18, 90),
                    Email = "emily.brendon@gmail.com",
                    Gender = "Female",
                    DebtRatio = 0.144616356M,
                    MonthlyIncome = new Random().Next(1500, 15000),
                    NumberOfDependents = new Random().Next(0, 3),
                    NumberOfRealEstateLoansOrLines = new Random().Next(0, 30),
                    NumberOfTime3059DaysPastDueNotWorse = new Random().Next(0, 4),
                    NumberOfTime6089DaysPastDueNotWorse = new Random().Next(0, 2),
                    NumberOfTimes90DaysLate = new Random().Next(0, 2),
                    NumerOfOpenCreditLinesAndLoans = new Random().Next(0, 20),
                    RevolvingUtilizationOfUnsecuredLines = 0M
                },
            };

            bot.VisitWebsite(commandLineArguments.Url);
            bot.InsertClientData(clientData);

            if (!commandLineArguments.InsertDataOnly)
            {
                //bot.GoToAboutPage();
                //bot.GoToPrivacyPage();
                bot.GoToHomePage();
                bot.GoToClientsPage();

                bot.DetailsFirstClientData();
                bot.PerformAnalysisFirstClientData();
                bot.DeleteAllClientData();
            }

            ChromeDriverManager.CloseDriverInstances();

            return Task.CompletedTask;
        }
    }
}

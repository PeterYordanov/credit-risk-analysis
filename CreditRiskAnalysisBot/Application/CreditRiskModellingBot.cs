﻿using CreditRiskAnalysisBot.Infrastructure;
using CreditRiskAnalysisBot.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Linq;

namespace CreditRiskAnalysisBot.Application
{
    public class CreditRiskModellingBot
    {
        private readonly WebDriverFacade webDriver = new WebDriverFacade(CreditRiskAnalysisBot.Enumerations.Browsers.Chrome);

        public void VisitWebsite(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                webDriver.GoToUrl(url);
            }
        }

        public void GoToAboutPage()
        {
            By by = By.LinkText("About");

            if (webDriver.IsElementPresent(by))
            {
                webDriver.FindElement(by).Click();
            }
        }

        public void GoToPrivacyPage()
        {
            By by = By.LinkText("Privacy");

            if (webDriver.IsElementPresent(by))
            {
                webDriver.FindElement(by).Click();
            }
        }

        public void GoToHomePage()
        {
            By by = By.LinkText("Home");

            if (webDriver.IsElementPresent(by))
            {
                webDriver.FindElement(by).Click();
            }
        }

        public void GoToClientsPage()
        {
            By by = By.LinkText("Clients");

            if (webDriver.IsElementPresent(by))
            {
                webDriver.FindElement(by).Click();
            }
        }

        public void InsertClientData(List<ClientData> clientData)
        {
            By createNewEntry = By.LinkText("Create New");

            foreach (var item in clientData)
            {
                GoToClientsPage();

                if (webDriver.IsElementPresent(createNewEntry))
                {
                    webDriver.FindElement(createNewEntry).Click();

                    webDriver.FindElement(By.Id("FirstName")).SendKeys(item.FirstName);
                    webDriver.FindElement(By.Id("LastName")).SendKeys(item.LastName);
                    webDriver.FindElement(By.Id("Age")).SendKeys(item.Age.ToString());
                    webDriver.FindElement(By.Id("Email")).SendKeys(item.Email);

                    if(!item.Gender.ToLower().Equals("male"))
                    {
                        SelectElement selectElement = new SelectElement(webDriver.FindElement(By.Id("Gender")));
                        selectElement.SelectByText("Female");
                    }

                    webDriver.FindElement(By.Id("MonthlyIncome")).SendKeys(item.MonthlyIncome.ToString());
                    webDriver.FindElement(By.Id("NumberOfDependents")).SendKeys(item.NumberOfDependents.ToString());
                    webDriver.FindElement(By.Id("DebtRatio")).SendKeys(item.DebtRatio.ToString());
                    webDriver.FindElement(By.Id("NumberOfOpenCreditLinesAndLoans")).SendKeys(item.NumerOfOpenCreditLinesAndLoans.ToString());
                    webDriver.FindElement(By.Id("RevolvingUtilizationOfUnsecuredLines")).SendKeys(item.RevolvingUtilizationOfUnsecuredLines.ToString());
                    webDriver.FindElement(By.Id("NumberOfTimes90DaysLate")).SendKeys(item.NumberOfTimes90DaysLate.ToString());
                    webDriver.FindElement(By.Id("NumberOfTime3059DaysPastDueNotWorse")).SendKeys(item.NumberOfTime3059DaysPastDueNotWorse.ToString());
                    webDriver.FindElement(By.Id("NumberOfTime6089DaysPastDueNotWorse")).SendKeys(item.NumberOfTime6089DaysPastDueNotWorse.ToString());
                    webDriver.FindElement(By.XPath("//input[@value='Save']")).Click();
                }
            }
        }

        public void DeleteAllClientData()
        {
            GoToClientsPage();

            By deleteAllLink = By.LinkText("Delete All");

            if (webDriver.IsElementPresent(deleteAllLink))
            {
                webDriver.FindElement(deleteAllLink).Click();
            }
        }

        public void DetailsFirstClientData()
        {
            GoToClientsPage();

            By detailsLinks = By.LinkText("Details");
            IWebElement firstDetailsButton = webDriver.FindElements(detailsLinks).FirstOrDefault();

            if (firstDetailsButton != null)
            {
                firstDetailsButton.Click();
            }
        }

        public void PerformAnalysisFirstClientData()
        {
            GoToClientsPage();

            By detailsLinks = By.LinkText("Perform Analysis");
            IWebElement firstDetailsButton = webDriver.FindElements(detailsLinks).FirstOrDefault();

            if (firstDetailsButton != null)
            {
                firstDetailsButton.Click();
            }
        }
    }
}
